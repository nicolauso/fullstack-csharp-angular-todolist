﻿using Newtonsoft.Json;
using System.Net.Http;
using System.Threading.Tasks;
using ToDoAPI.Models;

namespace ToDoAPI.Services
{
    public class MovieService
    {

        private readonly HttpClient _httpClient;

        public MovieService(HttpClient httpClient)
        {
            _httpClient = httpClient ?? throw new ArgumentNullException(nameof(httpClient));
        }

        public async Task<List<Movie>> GetMoviesFromApiAsync(string url)
        {
            List<Movie> movies = new List<Movie>();

            try
            {
                HttpResponseMessage response = await _httpClient.GetAsync(url);

                if (response.IsSuccessStatusCode)
                {
                   string responseBody = await response.Content.ReadAsStringAsync();
                    movies = JsonConvert.DeserializeObject<List<Movie>>(responseBody);
                   // liste d'objets Movie à partir de la réponse JSON
                    return movies;
                }
                else
                {
                    // Gérer les erreurs ou le code d'état non réussi ici
                    Console.WriteLine($"Erreur : {response.StatusCode}, {response.ReasonPhrase}");
                }
            }
            catch (HttpRequestException ex)
            {
                // Gérer les exceptions liées à la requête ici
                Console.WriteLine($"Erreur de requête HTTP : {ex.Message}");
            }

            return movies;
        }
    }
}
