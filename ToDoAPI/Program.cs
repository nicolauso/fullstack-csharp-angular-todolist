using Microsoft.EntityFrameworkCore;
using System.Configuration;
using TodoApi.Models;
using ToDoAPI.Utils; 

 
var builder = WebApplication.CreateBuilder(args); 


builder.Services.AddControllers();
builder.Services.AddDbContext<MovieContext>(opt =>
    opt.UseInMemoryDatabase("MovieList"));
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

builder.Services.AddCors(options =>
{
    options.AddPolicy("AllowAll", builder =>
            builder.AllowAnyOrigin()
                   .AllowAnyMethod()
                   .AllowAnyHeader());
});


var app = builder.Build();

builder.Configuration.AddJsonFile("apisettings.json",
        optional: true,
        reloadOnChange: true);

if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseDefaultFiles();
app.UseStaticFiles();

app.UseHttpsRedirection();

app.UseCors("AllowAll");

app.UseAuthorization();

app.MapControllers();

app.Run();