﻿using System;
using System.Collections.Generic;

namespace ToDoAPI.Models
{
    public class Movie
    {
        public long Id { get; set; }
        public bool Adult { get; set; }
        public int Budget { get; set; }
        public string[] Genres { get; set; }
        public string OriginalLanguage { get; set; }
        public string OriginalTitle { get; set; }
        public string Overview { get; set; }
        public int Popularity { get; set; }
        public string PosterPath { get; set; }

        public string ReleaseDate { get; set; } 

        public int Revenue { get; set; }
        public int Runtime { get; set; }
        public string Status { get; set; }

        public string TagLine { get; set; }
        public string Title { get; set; }
        public bool Video { get; set; }
        public int VoteAverage { get; set; }
        public int VoteCount { get; set; }
    }


}
