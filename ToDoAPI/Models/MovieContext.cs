﻿using Microsoft.EntityFrameworkCore;
using ToDoAPI.Models;

namespace TodoApi.Models;

public class MovieContext : DbContext
{
    public MovieContext(DbContextOptions<MovieContext> options)
        : base(options)
    {
    }

    public DbSet<Movie> Movie { get; set; } = null!;
}